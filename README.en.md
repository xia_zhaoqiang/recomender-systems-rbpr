# RBPR_RecomenderSystems

#### Description
This code implements a method called rating bayesian personalized ranking for recommender systems.

#### Software Architecture
All main codes are included in the RBPR python file. The demo.py shows how to use the RBPR method for training and testing this method.

#### Instructions

1. Usage: using import command to load the RBPR object; Call the RBPR object's training and testing method directly.
2. Example: two files in cold_start_dataset folder can be used to show the usage of RBPR method in demo.py.
3. Requirements: Numpy and some basic modules are neccesary.