from RBPR import *

if __name__ == '__main__':
    trainDataFile = 'cold_start_dataset\\train.txt'
    testDataFile = 'cold_start_dataset\\test.txt'
	resultSaveFile = 'RBPR_result.txt'
	read_file_without_scores(trainDataFile)
	iterations = 2000
	cf = RBPR(trainDataFile, resultSaveFile, '\t', 10, iterations)
	cf.train_rbpr()